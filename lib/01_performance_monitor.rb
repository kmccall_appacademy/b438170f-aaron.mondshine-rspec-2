def measure(r = 1)
  t1 = Time.now
  r.times { yield }
  r != 1 ? (Time.now - t1) / r : (Time.now - t1)
end
