def reverser
  reverse_arr = []
  yield.split.each { |word| reverse_arr << word.reverse }
  reverse_arr.join(" ")
end

def adder(y = 1)
  yield + y
end

def repeater(r = 1)
  r.times { yield }
end
